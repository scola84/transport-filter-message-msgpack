'use strict';

const msgpack = require('msgpack-js');
const Error = require('@scola/error');

class MsgPackFilter {
  receive(message) {
    return new Promise((resolve, reject) => {
      try {
        resolve(message.setBody(
          msgpack.decode(message.getBody())
        ));
      } catch (error) {
        reject(new Error('msgpack_decode_error', {
          origin: error,
          detail: {
            message
          }
        }));
      }
    });
  }

  send(message) {
    return new Promise((resolve, reject) => {
      try {
        resolve(message.setBody(
          msgpack.encode(message.getBody())
        ));
      } catch (error) {
        reject(new Error('msgpack_encode_error', {
          origin: error,
          detail: {
            message
          }
        }));
      }
    });
  }
}

module.exports = MsgPackFilter;
